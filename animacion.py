from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import os

def cargar_archivos(ruta):
   lista_archivos = os.listdir(ruta)
   os.chdir(ruta)
   lista_iteraciones = []
   dict_iteraciones = {}
   lista_maxx = []
   lista_minx= []
   lista_maxy = []
   lista_miny= []
   lista_maxz = []
   lista_minz= []
   i = 0
   for archivo in lista_archivos:
      iteracion = archivo.split("_")[0]
      datos_iteracion = np.loadtxt(str(lista_archivos[i]), skiprows=1, delimiter=",")
      x = datos_iteracion[:,0]
      y = datos_iteracion[:,1]
      z = datos_iteracion[:,2] 
      lista_maxx.append(max(x))
      lista_minx.append(min(x))
      lista_maxy.append(max(y))
      lista_miny.append(min(y))
      lista_maxz.append(max(z))
      lista_minz.append(min(z))
      dict_iteraciones[int(iteracion)] = {'coor x':x, 'coor y':y, 'coor z':z} 
      i += 1
      lista_iteraciones.append(int(iteracion))
   lista_iteraciones.sort()
   maxx = max(lista_maxx)
   minx = min(lista_minx)
   maxy = max(lista_maxy)
   miny = min(lista_miny)
   maxz = max(lista_maxz)
   minz = min(lista_minz)
   return [lista_iteraciones, maxx, minx, maxy, miny, maxz, minz, dict_iteraciones]

def animacion_archivos(lista_it_dict):
   fig = plt.figure("Animacion de datos")
   ax = plt.axes(projection='3d')
    # Dibujo el origen de coordenadas. Ejercicio: centro de masa del sistema
   ax.plot([0], [0], [0], 'g+')

   sp, = ax.plot([], [], [], 'r.')

   ax.set_xlim(min(lista_it_dict[2]), max(lista_it_dict[1]))
   ax.set_ylim(min(lista_it_dict[4]), max(lista_it_dict[3]))
   ax.set_zlim(min(lista_it_dict[6]), max(lista_it_dict[5]))

   def update(i):
      lista_iteraciones = lista_it_dict[0]
      dict_iteraciones = lista_it_dict[7]
      datos = dict_iteraciones[lista_iteraciones[i]]
      x = datos['coor x']
      y = datos['coor y']
      z = datos['coor z']
      sp.set_data(x, y)
      sp.set_3d_properties(z)
      return sp,

   ani = animation.FuncAnimation(fig, update, frames=len(dict_iteraciones[lista_iteraciones[0]]['coor x']), interval=20, repeat=False)
   plt.show()

