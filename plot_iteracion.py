#importo las librerias necesarias 
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt
import os

#creo funcion para plotear alguna iteracion como funcion de la ruta donde esta se encuentre para acortar codigo en script principal
def plot(ruta):
	datos = np.loadtxt(ruta, skiprows = 1, delimiter = ",")
	x = datos[:,0]
	y = datos[:,1]
	z = datos[:,2]
	nombre_archivo = os.path.basename(ruta)
	numero_iteracion = nombre_archivo.split("_")[0]
	grafico = plt.figure("Iteracion {numero}".format(numero=numero_iteracion))
	ax = plt.axes(projection="3d")
	graficar = ax.plot(x, y, z, "r.")
	return plt.show
